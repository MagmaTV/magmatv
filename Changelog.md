0.3.2
-----
 * Tests with bats.
 * Fix Makefile.

0.3.0
-----
 * BTTF migration.
 * Basic ZSH completion support.

0.2.6
-----
 * Cleaning CDATA from RSS.

0.2.2-4
-------
 * Minor fix on subtitles finder.

0.2.0
-----
 * Adding support for new versions of subliminal.
 * Adding debian directory.

0.1.8
-----
 * Adding suport for external parsers.
 * Adding support for local files.

0.1.6
-----
 * Some fixes on README file.
 * Checking feeds on updates.
 * Fix SSL downloads.

0.1.4
-----
 * Fix database creation.

0.1.2
-----
 * Control for torrent files.
 * Control peerflix errors.
 * Replace wget with curl.
