PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin

ifeq ($(PREFIX),/usr)
	SYSCONFDIR = /etc
else
	ifeq ($(PREFIX),/usr/local)
		SYSCONFDIR = /etc
	else
		SYSCONFDIR = $(PREFIX)/etc
	endif
endif

.PONHY: install uninstall tests

SHELL_COMPLETIONS = bash_completion
ifdef zsh
	SHELL_COMPLETIONS += zsh_completion
endif

install: script $(SHELL_COMPLETIONS)

script: magmatv
	install -d $(DESTDIR)$(BINDIR)
	install -m 755 magmatv $(DESTDIR)$(BINDIR)

bash_completion: magmatv.bash_completion
	install -d $(DESTDIR)$(SYSCONFDIR)/bash_completion.d
	install -m 644 magmatv.bash_completion $(DESTDIR)$(SYSCONFDIR)/bash_completion.d/magmatv

zsh_completion: _magmatv
	install -d $(DESTDIR)$(PREFIX)/share/zsh/site-functions
	install -m 644 _magmatv $(DESTDIR)$(PREFIX)/share/zsh/site-functions/_magmatv

uninstall:
	rm -f $(DESTDIR)$(BINDIR)/magmatv
	rm -f $(DESTDIR)$(SYSCONFDIR)/bash_completion.d/magmatv
	rm -f $(DESTDIR)$(PREFIX)/share/zsh/site-functions/_magmatv

tests:
	@test/libs/bats/bin/bats test
