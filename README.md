MagmaTV
======

_MagmaTV_ is a command-line tool for managing and playing television series and
films from RSS feeds.

Dependencies
------------
* `bash` _(version >= 4.0)_
* `sed`
* `curl`
* `xmllint` _(libxml2-utils)_
* `sqlite3`
* `fusermount` _(fuse)_
* `btfs`
* `mpv` _(Or another player)_
* `subliminal` _(version >= 0.9.0 Optional)_
* `notify-send` _(Optional)_

Installation
-------------
Can install the script on your system with:

    # PREFIX="/usr/local" make install

Or if you want zsh completion:

    # PREFIX="/usr/local" make zsh=true install

Usage
-----
Can execute the script with:

    $ magmatv <action>

### Actions

* Add a feed to database:

        $ magmatv add <type> <shortName> <url>

    Where `type` is `series` or `films`, `shortName` is a short name text
for the feed and `url` is the XML feed.

* Remove a feed from database:

        $ magmatv del <shortName|@id>

    Examples:

        $ magmatv del foo # Delete "Foo" feed from database.
        $ magmatv del @1 # Same but use feed ID.

* List the feeds from database:

        $ magmatv list

* Update the magnets and torrents from _XML feed_.

        $ magmatv update <all|type|shortName|@id>

    Examples:

        $ magmatv update all # Update all feed.
        $ magmatv update films # Update only films feed.
        $ magmatv update foo # Update only "Foo" feed.
        $ magmatv update @1 # Same but use feed ID.

* List episodes or films from a feed.

        $ magmatv list <shortName|@id|type>

* Set new shortName to a feed.

        $ magmatv rename <shortName|@id> <new shortName>

* Set new title to a feed.

        $ magmatv retitle <shortName|@id> <new title>

* Mark an episode as viewed.

        $ magmatv mark <shortName|@id> <episode>

    Examples:

        $ magmatv mark foo 1x1 # Mark only 1x1 as viewed.
        $ magmatv mark foo "<2x2" # Mark all episodes previus to 2x2.
        $ magmatv mark foo ">2x2" # Mark all episodes later to 2x2.

    You can use '<' or '>' prefix for specify all previous or later episodes.

* Unmark an episode as viewed.

        $ magmatv unmark <shortName|@id> <episode>

    Same format as `mark`.

* Play next pending episode of series.

        $ magmatv play <shortName|@id>

* Play a specific episode of series or a film.

        $ magmatv play <shortName|@id> <episode|film>

* List series with pending episodes.

        $ magmatv play

* Search a film.

        $ magmatv search <string>

* Show help message.

        $ magmatv help

Configuration
--------------
You can configure some options with `set` action.

        $ magmatv set <configuration> <value>

Some configurations are:

* `mplayer_command`: Command for call to multimedia player.
* `mplayer_options`: Options for multimedia player.
* `mplayer_secondary_subtitle_options`: Options for play secondary subtitle.
* `mplayer_subtitle_options`: Options for displaying subtitle on player.
* `notify_options`: Options for notify_send.
* `notify_summary`: Summary for notifications.
* `preferedhd`: Play 720p videos when available if true.
* `secondary_subtitle`: Language code for secondary subtitle or `none` for disable it.
* `subtitle_dir`: Directory for save the subtitles.
* `automark`: Automatically mark the 'films', 'series', 'all', 'none' when played, or 'ask'.
* `notify`: Notify for news episodes if `true`.
* `subtitle_fail`: `error` and exit when subtitle download fail or only `warning`.
* `subtitle`: Language code for subtitle or `none` for disable it.

For displays all configurations and values:

    $ magmatv config

