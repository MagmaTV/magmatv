#!./test/libs/bats/bin/bats

load _helper

@test "set config invalid arguments" {
    run set_config foo
    [[ $status -eq 1 ]]
    [[ $output == "[!!] Invalid arguments for \"set\" action." ]]
}

@test "set config mplayer_command" {
    run set_config mplayer_command mpv
    [[ $status -eq 0 ]]
    run get_config
    [[ $status -eq 0 ]]
    [[ $(grep "^mplayer_command " <<< "${output}") == "mplayer_command \"mpv\"" ]]
}

@test "set config notify" {
    run set_config notify false
    [[ $status -eq 0 ]]
    run set_config notify true
    [[ $status -eq 0 ]]
    run set_config notify foo
    [[ $status -eq 1 ]]
}

@test "set config preferedhd" {
    run set_config preferedhd false
    [[ $status -eq 0 ]]
    run set_config preferedhd true
    [[ $status -eq 0 ]]
    run set_config preferedhd foo
    [[ $status -eq 1 ]]
}

@test "set config subtitle_fail" {
    run set_config subtitle_fail error
    [[ $status -eq 0 ]]
    run set_config subtitle_fail warning
    [[ $status -eq 0 ]]
    run set_config subtitle_fail foo
    [[ $status -eq 1 ]]
}
