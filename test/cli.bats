#!./test/libs/bats/bin/bats

load _helper

@test "echo no color (ok, warn, error, sub, end)" {
    unset QUIETALL
    unset QUIET
    local TERM="rxvt-unicode-256color"
    local NOCOLOR="true"
    run echook "foo"
    [[ $status -eq 0 ]]
    [[ $output = "[ok] foo" ]]
    run echowarn "foo"
    [[ $status -eq 0 ]]
    [[ $output = "[ww] foo" ]]
    run echoerror "foo"
    [[ $status -eq 0 ]]
    [[ $output = "[!!] foo" ]]
    run echosub "foo"
    [[ $status -eq 0 ]]
    [[ $output = "     ├─ foo" ]]
    run echoend "foo"
    [[ $status -eq 0 ]]
    [[ $output = "     └─ foo" ]]
}

@test "echo color (rxvt, xterm)" {
    unset NOCOLOR
    unset QUIET
    unset QUIETALL
    local TERM="rxvt-unicode-256color"
    run echook "foo"
    [[ $status -eq 0 ]]
    [[ $output == "$(echo -ne '[\e[1;32mok\e[0m] foo')" ]]
    local TERM="xterm-256color"
    run echook "foo"
    [[ $status -eq 0 ]]
    [[ $output == "$(echo -ne '[\e[1;32mok\e[0m] foo')" ]]
    local TERM="st-256color"
    run echook "foo"
    [[ $status -eq 0 ]]
    [[ $output == "$(echo -ne '[\e[1;32mok\e[0m] foo')" ]]
    unset TERM
    run echook "foo"
    [[ $status -eq 0 ]]
    [[ $output == "[ok] foo" ]]
    local TERM="bar"
    run echook "foo"
    [[ $status -eq 0 ]]
    [[ $output == "[ok] foo" ]]
}

@test "echo quiet (ok, warn, error, sub, end)" {
    unset QUIETALL
    local QUIET="true"
    local NOCOLOR="true"
    run echook "foo"
    [[ $status -eq 0 ]]
    [[ -z $output ]]
    run echowarn "foo"
    [[ $status -eq 0 ]]
    [[ -z $output ]]
    run echoerror "foo"
    [[ $status -eq 0 ]]
    [[ $output == "[!!] foo" ]]
    run echosub "foo"
    [[ $status -eq 0 ]]
    [[ -z $output ]]
    run echoend "foo"
    [[ $status -eq 0 ]]
    [[ -z $output ]]
}

@test "echo quietall (ok, warn, error, sub, end)" {
    unset QUIET
    local QUIETALL="true"
    local NOCOLOR="true"
    run echook "foo"
    [[ $status -eq 0 ]]
    [[ -z $output ]]
    run echowarn "foo"
    [[ $status -eq 0 ]]
    [[ -z $output ]]
    run echoerror "foo"
    [[ $status -eq 0 ]]
    [[ -z $output ]]
    run echosub "foo"
    [[ $status -eq 0 ]]
    [[ -z $output ]]
    run echoend "foo"
    [[ $status -eq 0 ]]
    [[ -z $output ]]
}
