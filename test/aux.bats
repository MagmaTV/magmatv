#!./test/libs/bats/bin/bats

load _helper

@test "check dependencies" {
    run check_dep "foo"
    [[ $status -eq 1 ]]
    [[ $output = "[!!] Command \"foo\" not found." ]]
    run check_dep "bash"
    [[ $status -eq 0 ]]
}
