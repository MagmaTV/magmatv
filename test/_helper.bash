#!/usr/bin/env bash

export TESTS_DIR="$BATS_TEST_DIRNAME"
export MAGMATV_PATH="$(cd "$TESTS_DIR/.." && pwd)"
export NOCOLOR="true"
export XDG_CONFIG_DIR="/tmp/magmatv/test/config"
export XDG_DATA_HOME="/tmp/magmatv/test/data"

setup() {
    local QUIET="true"
    source "${MAGMATV_PATH}/magmatv"
    unset QUIET
    [[ ! -d "/tmp/magmatv.test" ]] && mkdir -p "/tmp/magmatv.test"
}

teardown() {
    [[ -f "${CONFIG}" ]] && rm "${CONFIG}"
    [[ -f "${DATABASE}" ]] && rm "${DATABASE}"
}
