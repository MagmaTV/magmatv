#!./test/libs/bats/bin/bats

load _helper

@test "parse title" {
    run parse_title "file://${BATS_TEST_DIRNAME}/feeds/good_serie_data.xml"
    echo "$output"
    [[ $output == "Foo RSS" ]]
    [[ $status -eq 0 ]]
}

@test "series parser" {
    run add series foo file://${BATS_TEST_DIRNAME}/feeds/good_serie_data.xml
    [[ $status -eq 0 ]]
    run list_episodes foo
    [[ $status -eq 0 ]]
    [[ ${#line[@]} -eq 0 ]]
    run update_feed foo
    [[ $status -eq 0 ]]
    run list_episodes foo
    echo "${#lines[@]}"
    [[ ${#lines[@]} -eq 3 ]]
}
